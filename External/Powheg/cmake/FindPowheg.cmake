# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Locate Powheg
# Defines:
#  - POWHEGPATH

# now located in:
set( _pwg_otf_home "/afs/cern.ch/atlas/offline/external/powhegbox/ATLASOTF-00-04-00" )

find_path( POWHEGPATH NAMES ListOfAvailable.txt
           PATHS ${_pwg_otf_home} )
mark_as_advanced( POWHEGPATH )
set( POWHEGPATH ${POWHEGPATH} )

mark_as_advanced( POWHEG_OTF_FOUND )

# If Powheg was found, set up some environment variables for it:
if( POWHEG_OTF_FOUND )

   # Set the environment:
   set( POWHEG_ENVIRONMENT
      set POWHEGPATH "${_pwg_otf_home}"
   )

   unset( _pwg_otf_home )

   # Set up a dependency on the POWHEG data RPM:
#   set_property( GLOBAL APPEND PROPERTY ATLAS_EXTERNAL_RPMS
#      "ATLAS_Geant4Data_${G4_VERSION}" )

endif()
