# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building ROOT as part of the offline / analysis release.
#

# Set the name of the package:
atlas_subdir( ROOT )

# In release recompilation mode stop now:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_ROOT )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building ROOT as part of this project" )

# The source code of ROOT:
set( _rootSource "https://root.cern.ch/download/root_v6.12.06.source.tar.gz" )
set( _rootMd5 "bef6535a5d0cdf471b550da45a10f605" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ROOTBuild )

# Extra arguments for the CMake configuration of the ROOT build:
set( _extraArgs )
if( APPLE )
   list( APPEND _extraArgs -Dvc:BOOL=OFF -Drpath:BOOL=ON )
endif()
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=Release )
elseif( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# The build needs Python 2.7:
if( ATLAS_BUILD_PYTHON )
   list( APPEND _extraArgs
      -DPYTHON_EXECUTABLE:PATH=${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python
      -DPYTHON_INCLUDE_DIR:PATH=${CMAKE_INCLUDE_OUTPUT_DIRECTORY}/python2.7
      -DPYTHON_LIBRARY:PATH=${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${CMAKE_SHARED_LIBRARY_PREFIX}python2.7${CMAKE_SHARED_LIBRARY_SUFFIX} )
else()
   find_package( PythonInterp 2.7 REQUIRED )
   find_package( PythonLibs 2.7 REQUIRED )
   list( APPEND _extraArgs -DPYTHON_EXECUTABLE:PATH=${PYTHON_EXECUTABLE}
      -DPYTHON_INCLUDE_DIR:PATH=${PYTHON_INCLUDE_DIRS}
      -DPYTHON_LIBRARY:PATH=${PYTHON_LIBRARIES} )
endif()

# ...and XRootD. Note that if we are not building XRootD ourselves, we
# just leave it up to ROOT to find it the best that it can.
if( ATLAS_BUILD_XROOTD )
   list( APPEND _extraArgs
      -Dxrootd:BOOL=ON
      -DXROOTD_ROOT_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
endif()

# ...and optionally DCAP.
if( ATLAS_BUILD_DCAP )
   list( APPEND _extraArgs
      -Ddcache:BOOL=ON -DDCAP_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
endif()

# ...and optionally Davix.
if( ATLAS_BUILD_DAVIX )
   list( APPEND _extraArgs
      -Ddavix:BOOL=ON -DDAVIX_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
endif()

# ...and optionally LibXml2.
if( ATLAS_BUILD_LIBXML2 )
   list( APPEND _extraArgs
      -DCMAKE_INCLUDE_PATH:PATH=${CMAKE_INCLUDE_OUTPUT_DIRECTORY}
      -DCMAKE_LIBRARY_PATH:PATH=${CMAKE_LIBRARY_OUTPUT_DIRECTORY} )
endif()

# Build ROOT:
ExternalProject_Add( ROOT
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_rootSource}
   URL_MD5 ${_rootMd5}
   PATCH_COMMAND patch -p1 <
   ${CMAKE_CURRENT_SOURCE_DIR}/patches/v6-12-06-blas.patch
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -Dall:BOOL=ON -Dbuiltin_gsl:BOOL=ON
   -Dbuiltin_freetype:BOOL=ON -Dbuiltin_fftw3:BOOL=ON -Dbuiltin_lzma:BOOL=ON
   -Dcxx14:BOOL=ON ${_extraArgs}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( ROOT forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of ROOT. "
   DEPENDERS download )
ExternalProject_Add_Step( ROOT purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for ROOT"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( ROOT forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f <BINARY_DIR>/CMakeCache.txt
   COMMENT "Forcing the configuration of ROOT"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( ROOT buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing ROOT into the build area"
   DEPENDEES install )
add_dependencies( Package_ROOT ROOT )
if( ATLAS_BUILD_PYTHON )
   add_dependencies( ROOT Python )
endif()
if( ATLAS_BUILD_XROOTD )
   add_dependencies( ROOT XRootD )
endif()
if( ATLAS_BUILD_DCAP )
   add_dependencies( ROOT dcap )
endif()
if( ATLAS_BUILD_DAVIX )
   add_dependencies( ROOT Davix )
endif()
if( ATLAS_BUILD_LIBXML2 )
   add_dependencies( ROOT LibXml2 )
endif()

# Install ROOT:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
