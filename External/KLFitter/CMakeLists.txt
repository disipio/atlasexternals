# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building KLFitter as part of an analysis release
#

# The name of the package:
atlas_subdir( KLFitter )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Set ROOTSYS based on the build environment:
if( ATLAS_BUILD_ROOT )
   set( _rootsys ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
else()
   find_package( ROOT REQUIRED )
   set( _rootsys ${ROOTSYS} )
endif()

# Set the root directory of the ATLAS-built BAT version and
# forward it to the KLFitter cmake config.
set( _bat_root ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )

# KLFitter source
set( _git_repo https://github.com/KLFitter/KLFitter.git )
set( _version_tag "v1.0.0" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/KLFitterBuild )

# Build KLFitter for the build area:
ExternalProject_Add( KLFitter
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   GIT_REPOSITORY ${_git_repo}
   GIT_TAG ${_version_tag}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DROOT_ROOT:PATH=${_rootsys}
   -DBAT_ROOT:PATH=${_bat_root}
   -DINSTALL_EXAMPLES:BOOL=FALSE
   LOG_CONFIGURE 1
   )
ExternalProject_Add_Step( KLFitter buildinstall
  COMMAND ${CMAKE_COMMAND} -E remove_directory
   ${_buildDir}/cmake
  COMMAND ${CMAKE_COMMAND} -E copy_directory
  ${_buildDir}/ <INSTALL_DIR>
  COMMENT "Installing KLFitter into the build area"
  DEPENDEES install
  )

add_dependencies( Package_KLFitter KLFitter )
add_dependencies( KLFitter BAT )
if( ATLAS_BUILD_ROOT )
   add_dependencies( KLFitter ROOT )
endif()

# Install KLFitter:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

install( FILES cmake/FindKLFitter.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules OPTIONAL )
