# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Package building ACTS as part of the offline / analysis software build.
#

# The name of the package:
atlas_subdir( ACTS )

find_package( Eigen )
find_package( Boost 1.62.0 REQUIRED )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Stop if the build is not needed:
#if( NOT ATLAS_BUILD_ACTS )
#   return()
#endif()

# Tell the user what's happening:
message( STATUS "Building ACTS as part of this project" )

# The source of ACTS:
# This is commented out for now, since we will temporarily build from ACTS master branch.
# set( _ACTS_Source
#    "http://acts.web.cern.ch/ACTS/v0.05.03/ACTS-v0.05.03.tar.gz" )
# set( _ACTS_Md5 "3320b5c16bd6e2599309153d518f368e" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ACTSBuild )

# Build ACTS for the build area:
ExternalProject_Add( ACTS
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   # URL ${_ACTS_Source}
   # URL_MD5 ${_ACTS_Md5}
   GIT_REPOSITORY https://gitlab.cern.ch/acts/acts-core.git # Temporarily build from ACTS master branch
   GIT_TAG "master"
   
   CMAKE_CACHE_ARGS 
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DBOOST_ROOT:PATH=${BOOST_ROOT}
   -DEIGEN_INCLUDE_DIR:PATH=${EIGEN_INCLUDE_DIRS}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( ACTS purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for ACTS"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( ACTS buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing ACTS into the build area"
   DEPENDEES install )
add_dependencies( Package_ACTS ACTS )

# And now install it:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
