# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building GDB as part of the offline software build.
#

# The name of the package:
atlas_subdir( Gdb )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Set up a dependency on Python:
find_package( PythonInterp 2.7 REQUIRED )
find_package( PythonLibs 2.7 REQUIRED )
find_package( ZLIB )
find_package( xz )

# Temporary build directory:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/GdbBuild )

# Contents of the configuration script. Note that it's extremely difficult
# to use quotes of any kind inside of ExternalProject_Add, in conjunction
# with the way we log the build messages in the nightly. So just constructing
# a script that executes the configuration for us, is the most robust thing
# to do.
set( _cmd "sh -c \"./configure --prefix=${_buildDir}" )
set( _cmd "${_cmd} --with-system-gdbinit=${_buildDir}/etc/gdbinit" )
set( _cmd "${_cmd} --with-python=${PYTHON_ROOT} --enable-install-libiberty" )
set( _cmd "${_cmd} --with-liblzma-prefix=${XZ_ROOT}" )
set( _cmd "${_cmd} LDFLAGS=-L${PYTHON_ROOT}/lib" )
set( _cmd "${_cmd} CFLAGS=\\\"-fPIC -Wno-implicit-function-declaration\\\"" )
set( _cmd "${_cmd} CXXFLAGS=-fPIC\"" )
file( GENERATE
   OUTPUT ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh
   CONTENT ${_cmd} )

# Libraries to link against:
set( _libraries
  ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${CMAKE_STATIC_LIBRARY_PREFIX}bfd${CMAKE_STATIC_LIBRARY_SUFFIX} )
list( APPEND _libraries
  ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${CMAKE_STATIC_LIBRARY_PREFIX}iberty${CMAKE_STATIC_LIBRARY_SUFFIX} )

# Set up the build of GDB for the build area:
ExternalProject_Add( Gdb
   PREFIX ${CMAKE_BINARY_DIR}
   URL http://cern.ch/atlas-software-dist-eos/externals/Gdb/gdb-8.1.tar.xz
   URL_MD5 f46487561f9a16916a8102316f7fd105
   PATCH_COMMAND 
   cat ${CMAKE_CURRENT_SOURCE_DIR}/patches/gdb-8.0.patch
       ${CMAKE_CURRENT_SOURCE_DIR}/patches/gdb-bug20020.patch
       | patch -p0
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND
   sh ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh
   BUILD_COMMAND make
   COMMAND make install
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir}/ <INSTALL_DIR>
   BUILD_BYPRODUCTS ${_libraries}
   )
add_dependencies( Package_Gdb Gdb )

# Set up its installation:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES cmake/Findgdb.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )

# Build the package's application:
atlas_add_executable( resolveAtlasAddr2Line src/resolveAtlasAddr2Line.cxx
   INCLUDE_DIRS ${CMAKE_INCLUDE_OUTPUT_DIRECTORY} ${ZLIB_INCLUDE_DIRS}
   LINK_LIBRARIES ${_libraries} ${ZLIB_LIBRARIES} )
add_dependencies( resolveAtlasAddr2Line Gdb )

# Install its other resources:
atlas_install_python_modules( python/__init__.py python/gdbhacks )
atlas_install_scripts( scripts/atlasAddress2Line )
