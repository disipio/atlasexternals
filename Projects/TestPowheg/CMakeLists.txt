# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Main configuration file for building the TestPowheg project.
#

# The minimum required CMake version:
cmake_minimum_required( VERSION 3.2 FATAL_ERROR )

# If the user didn't specify AtlasCMake_DIR explicitly, pick up that code
# from this repository:
if( "${AtlasCMake_DIR}" STREQUAL "" AND "$ENV{AtlasCMake_DIR}" STREQUAL "" )
   set( AtlasCMake_DIR ${CMAKE_SOURCE_DIR}/../../Build/AtlasCMake )
endif()

# If the user didn't specify LCG_DIR explicitly, pick up the code from this
# repository:
if( "${LCG_DIR}" STREQUAL "" AND "$ENV{LCG_DIR}" STREQUAL "" )
   set( LCG_DIR ${CMAKE_SOURCE_DIR}/../../Build/AtlasLCG )
endif()

# Read in the project's version from a file called version.txt. But let it be
# overridden from the command line if necessary.
file( READ ${CMAKE_SOURCE_DIR}/version.txt _version )
string( STRIP ${_version} _version )
set( TESTPOWHEG_PROJECT_VERSION ${_version}
   CACHE STRING "Version of the TestPowheg project to build" )
unset( _version )

# Find the ATLAS CMake code:
find_package( AtlasCMake REQUIRED )

# Set up the AtlasLCG modules, without setting up an actual LCG release.
# This is necessary to have the AtlasLCG code installed together with the
# project. We don't actually need the LCG code for the test project's build.
set( LCG_VERSION_POSTFIX "" )
set( LCG_VERSION_NUMBER 0 )
find_package( LCG ${LCG_VERSION_NUMBER} REQUIRED )

# Set up CTest:
atlas_ctest_setup()

# Declare project name and version
atlas_project( TestPowheg ${TESTPOWHEG_PROJECT_VERSION} )

# Install the export sanitizer script:
install( FILES ${CMAKE_SOURCE_DIR}/atlas_export_sanitizer.cmake.in
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules/skeletons )

# Generate the environment setup for the externals, to be used during the build:
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )

# Now generate and install the installed setup files:
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/env_setup_install.sh )
install( FILES ${CMAKE_BINARY_DIR}/env_setup_install.sh
   DESTINATION . RENAME env_setup.sh )

# Package up the release using CPack:
atlas_cpack_setup()
