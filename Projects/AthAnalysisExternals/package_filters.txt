# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthAnalysisExternals.
#
+ External/BAT
+ External/Davix
+ External/Eigen
+ External/FastJet
+ External/FastJetContrib
+ External/GPerfTools
+ External/GoogleTest
+ External/HDF5
+ External/KLFitter
+ External/Lhapdf
+ External/lwtnn
+ External/CheckerGccPlugins
+ External/CLHEP
+ External/XRootD
- .*
